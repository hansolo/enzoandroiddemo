package eu.hansolo.enzoandroiddemo.app;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import eu.hansolo.enzo.*;
import eu.hansolo.enzo.Lcd.Design;

import java.util.Random;


public class MainActivity extends Activity {
    private static final Random         RND = new Random();
    private              Lcd            lcd;
    private              Led            led;
    private              SimpleGauge    gauge;
    private              OneEightyGauge gauge1;
    private              AvGauge        gauge2;
    private              Gauge          gauge3;
    private              FlatGauge      gauge4;
    private              SevenSegment   sevenSegment;
    private              SixteenSegment sixteenSegment;
    private              Handler        handler;
    private              Runnable       runnable;


    @Override public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lcd    = (Lcd) findViewById(R.id.lcd1);
        lcd.setAlarmVisible(true);
        lcd.setThreshold(25f);
        lcd.setThresholdVisible(true);
        lcd.setDesign(Design.BLUE_LIGHTBLUE);
        lcd.setFormerValueVisible(true);

        led    = (Led) findViewById(R.id.led);
        gauge  = (SimpleGauge) findViewById(R.id.gauge);
        gauge1 = (OneEightyGauge) findViewById(R.id.gauge1);
        gauge1.getGradientLookup().setStops(new Stop(0.00000f, Color.rgb(53, 82, 160)),
                                            new Stop(0.09090f, Color.rgb(69, 106, 207)),
                                            new Stop(0.18181f, Color.rgb(69, 161, 207)),
                                            new Stop(0.27272f, Color.rgb( 69, 207, 109)),
                                            new Stop(0.36363f, Color.rgb(154, 219, 73)),
                                            new Stop(0.50909f, Color.rgb(227, 235, 79)),
                                            new Stop(0.60000f, Color.rgb(239, 215, 80)),
                                            new Stop(0.63636f, Color.rgb(239, 183, 80)),
                                            new Stop(0.67272f, Color.rgb(239, 152, 80)),
                                            new Stop(0.74545f, Color.rgb(239, 96, 80)),
                                            new Stop(1.00000f, Color.rgb(165,  66,  55)));
        gauge2 = (AvGauge) findViewById(R.id.gauge2);
        gauge2.getOuterGradientLookup().setStops(new Stop(0.00000f, Color.rgb(0, 0, 255)),
                                                 new Stop(1.00000f, Color.rgb(255, 0, 0)));
        gauge2.getInnerGradientLookup().setStops(new Stop(0.00000f, Color.rgb(0, 0, 255)),
                                                 new Stop(1.00000f, Color.rgb(255, 0, 0)));
        gauge3 = (Gauge) findViewById(R.id.gauge3);
        gauge3.setSections(new Section(20, 40, Color.argb(128, 0, 200, 50)));
        gauge3.setAreas(new Section(30, 60, Color.argb(128, 0, 50, 200)));

        sevenSegment = (SevenSegment) findViewById(R.id.sevenSegment);
        sevenSegment.setSegmentColor(Color.rgb(200, 0, 255));

        sixteenSegment = (SixteenSegment) findViewById(R.id.sixteenSegment);
        sixteenSegment.setSegmentColor(Color.rgb(0, 200, 255));

        gauge4 = (FlatGauge) findViewById(R.id.gauge4);
        gauge4.setBarBackgroundColor(Color.TRANSPARENT);
        gauge4.setTitleColor(Color.WHITE);
        gauge4.setValueColor(Color.WHITE);
        gauge4.setUnitColor(Color.WHITE);

        led.setBlinking(true);

        runnable = new Runnable() {
            @Override public void run() {
                lcd.setValue(RND.nextInt(100));
                gauge.setValue(RND.nextInt(100));
                gauge1.setValue(RND.nextInt(100));
                gauge2.setOuterValue(RND.nextInt(100));
                gauge2.setInnerValue(RND.nextInt(100));
                gauge3.setValue(RND.nextInt(100));
                gauge4.setValue(RND.nextInt(100));
                led.setOn(!led.isOn());
                sevenSegment.setCharacter((Integer.toString(RND.nextInt(9))).charAt(0));
                sixteenSegment.setCharacter(Character.toString ((char) RND.nextInt(48 + 42)).charAt(0));
                handler.postDelayed(runnable, 3000);
            }
        };
        startHandler();
    }

    public void startHandler() {
        handler = new Handler();
        handler.postDelayed(runnable, 3000);
    }
    public void stopHandler() {
        handler.removeCallbacks(runnable);
    }
}

